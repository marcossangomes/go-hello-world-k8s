Ao receber o desáfio priorizei as ferramentas Indicadas.

Quis trabalhar com kubernets desde que recebi o desáfio, nenhuma outra solução que não envolvesse Kubernets foi avaliada.

Tive que assimilar o funcionamento do cluster e de algumas ferramentas que podem trabalhar em conjunto | Jenkis, Spinnaker, Helm, Flux, Gitlab |.

Gastei grande parte do meu tempo para chegar em um terraform que privisionasse o cluster e estudando uma maneira de realizar um CD para a aplicação.

Dúvidas á disposição.
# App Eks

### - Crie um bucket:

![node-to-node](img/Selection_001.png)

### - Baixe o repositório

git clone https://gitlab.com/marcossangomes/tf-eks

O terraform.state já se encontra sincronizado com o s3.

![node-to-node](img/Selection_002.png)

### Execute:

- terraform init
- terraform apply -var-file=variables.tfvars -auto-approve

Sincronize seu cluster: 

![node-to-node](img/Selection_009.png)

### 1) Crie um repositório no gitlab

### 2) Entre no repositório e vá em Settings > CI/CD > Variables.

### 3) Cadastre as varíáveis de ambiente.

![node-to-node](img/Selection_008.png)

### 4) Cadastre seu usuário e senha docker, assina-le a opção de projeger a senha.

### 5) Cadastre SERVER com o server do seu cluster.

![node-to-node](img/Selection_012.png)


Obs: Como são informações sensíveis não tirei o print do dado inteiramente, esses dados não devem ser expostos.

### 6) Cadastre CERTIFICATE_AUTHORITY_DATA, assina-le a opção de projeger a senha.

![node-to-node](img/Selection_013.png)


Obs: Como são informações sensíveis não tirei o print do dado inteiramente, esses dados não devem ser expostos.

### 7) Cadastre o secret token, para pega-lo:

![node-to-node](img/Selection_014.png)


### 8 ) Crie dois repositórios Docker para o build do frontend da aplicação e do backend.


![node-to-node](img/Selection_015.png)


### 9 ) Arquivo de configuração do github com cluster.


![node-to-node](img/Selection_016.png)


### 10 ) Arquivo da aplicação com aplicação, dockerfile do frontend e dockerfile do backend.


![node-to-node](img/Selection_017.png)


### 11) Arquivo de Deploy da aplicação


![node-to-node](img/Selection_018.png)


### 12 ) Arquivo do pipeline onde será executado o build e o deploy da app.


![node-to-node](img/Selection_019.png)


### 13) Comite ou starte o pipeline em CICD > Pipelines.


![node-to-node](img/Selection_021.png)


- O processo de build irá buildar duas imagens, como se fossem um dockerfile do frontend e do backend e fará o push no dockerhub.
(As imagens que foram buildadas nesse pipeline não são da aplicação, não localizei o código da aplicação então usei um código com dockerfile para ilustração.)

- O processo de deploy também é realizado no pipeline.

### 14) Crie um hosted zone.


![node-to-node](img/Selection_022.png)


### 15) O meu domínio no caso foi adquirido pelo GoDaddy. Altere servidor de dns no Godaddy.


![node-to-node](img/Selection_023.png)


### 16) Crie um recordset e aponte para o loadbalancer em 'Alias Target'



![node-to-node](img/Selection_024.png)



### 17) Pipeline OK .



![node-to-node](img/Selection_025.png)



![node-to-node](img/Selection_026.png)



### 18 ) acesse www.marcossangomes.live para acessar a aplicação



![node-to-node](img/Selection_027.png)


