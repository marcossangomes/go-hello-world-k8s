#!/bin/bash

#Deploy do Redis master
kubectl apply -f https://gitlab.com/marcossangomes/go-hello-world-k8s/-/raw/master/app/redis-master-deployment.yaml

#Criando o serviço Redis Master (comunicação da app com o redis para gravação dos dados)
kubectl apply -f https://gitlab.com/marcossangomes/go-hello-world-k8s/-/raw/master/app/redis-master-service.yaml 

#Deploy do Redis slave
kubectl apply -f https://gitlab.com/marcossangomes/go-hello-world-k8s/-/raw/master/app/redis-slave-deployment.yaml

#Criando o serviço Redis Slave(comunicação da app com o redis para a leitura dos dados)
kubectl apply -f https://gitlab.com/marcossangomes/go-hello-world-k8s/-/raw/master/app/redis-slave-service.yaml

#Deploy do Frontend
kubectl apply -f https://gitlab.com/marcossangomes/go-hello-world-k8s/-/raw/master/app/frontend-deployment.yaml

#Criando o serviço do Frontend
kubectl apply -f https://gitlab.com/marcossangomes/go-hello-world-k8s/-/raw/master/app/frontend-service-eks.yaml

#Visualizando o resultado
kubectl get all